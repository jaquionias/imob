@extends('web.master.master')
@section('content')
    <div class="main_contact py-5 bg-light text-center">
        <div class="container">
            <h1 class="text-front">Entre em Contato Conosco</h1>
            <p class="mb-0">Quer conversar com um corretor exclusivo e ter o atendimento diferenciado em busca do seu imóvel
                dos sonhos?</p>
            <p>Preencha o formulário abaixo e vamos lhe direcionar para alguém que entende a sua necessidade!</p>

            <div class="row text-left">
                <form action="{{route('web.sendEmail')}}" method="post">
                    @csrf
                    <h2 class="icon-envelope text-black-50">Envie um e-mail</h2>
                    @if(session('message'))
                        <p class="trigger {{session('type')}}">{!! session('message') !!}</p>
                    @endif
                    @if($errors->all())
                        <p class="trigger error">
                            <i class="icon-frown-o"></i>
                            <br><br>
                            <strong>Oooops!!! Há algo errado.</strong><br/><br/>

                            @if(!old('name'))
                                Para enviar essa mensagem, preencha o(s) campo(s):<br/>
                            @else
                                <?php
                                $primeiroNome = explode(" ", old('name'));
                                ?>
                                {{$primeiroNome[0]}}, para enviar essa mensagem preencha o(s) campo(s):<br/>
                            @endif
                            @if(!old('name'))
                                <strong>'Seu nome'</strong> |
                            @endif
                            @if(!old('cell'))
                                <strong>'Seu telefone'</strong> |
                            @endif
                            @if(!old('email'))
                                <strong>'Seu e-mail'</strong> |
                            @endif
                            @if(!old('message'))
                                <strong>'Sua mensagem'</strong>
                            @endif

                        </p>
                    @endif
                    <p class="text-front"><small>Os campos com (*) são obrigatórios!</small></p>
                    <div class="form-group">
                        <input type="text" value="{{old('name')}}" name="name" class="form-control" placeholder="Insira seu nome">
                    </div>

                    <div class="form-group">
                        <input type="text" value="{{old('cell')}}" name="cell" class="form-control" placeholder="Insira seu telefone">
                    </div>

                    <div class="form-group">
                        <input type="email" value="{{old('email')}}" name="email" class="form-control" placeholder="Insira seu melhor e-mail">
                    </div>

                    <div class="form-group">
                        <textarea name="message"  rows="5" class="form-control" placeholder="Escreva sua mensagem...">{{old('message')}}</textarea>
                    </div>

                    <div class="form-group text-right">
                        <button class="btn btn-front">Enviar Contato</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="main_contact_types bg-white p-5">
        <div class="container">
            <div class="row text-center">
                <div class="col-12 col-md-4">
                    <h2 class="icon-envelope">Por E-mail</h2>
                    <p>Nossos atendentes irão entrar em contato com você assim que possível.</p>
                    <p class="pt-2"><a href="mailto:contato@laradev.com.br" class="text-front">contato@laradev.com.br</a></p>
                </div>

                <div class="col-12 col-md-4">
                    <h2 class="icon-phone">Por Telefone</h2>
                    <p>Estamos disponíveis nos números abaixo no horário comercial.</p>
                    <p class="pt-2 text-front">+55 (48) 3322-1234</p>
                </div>

                <div class="col-12 col-md-4">
                    <h2 class="icon-share-alt">Redes Sociais</h2>
                    <p>Fique por dentro do tudo o que a gente compartilha em nossas redes sociais!</p>
                    <p><button class="btn btn-front icon-facebook icon-notext"></button> <button class="btn btn-front icon-twitter icon-notext"></button> <button class="btn btn-front icon-instagram icon-notext"></button></p>
                </div>
            </div>
        </div>
    </div>
@endsection
