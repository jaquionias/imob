@extends('web.master.master')
@section('content')
    <h2 class="text-front icon-paint-brush text-center">Seu e-mail foi enviado com sucesso! Em breve entraremos em contato.</h2>
    <p class="text-center"><a href="{{url()->previous()}}" class="text-front">...Continuar navegando!</a></p>
@endsection
