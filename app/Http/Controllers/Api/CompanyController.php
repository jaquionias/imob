<?php

namespace Imob\Http\Controllers\Api;

use Illuminate\Http\Request;
use Imob\Company;
use Imob\Http\Controllers\Controller;

class CompanyController extends Controller
{

    public function index()
    {
        $companies = Company::all();
        return response()->json($companies);
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
