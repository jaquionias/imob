<?php

namespace Imob\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Imob\Http\Controllers\Controller;
use Imob\Http\Requests\Web\ContactRequest;
use Imob\Mail\Web\Contact;
use Imob\Property;

class WebController extends Controller
{
    public function home()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - TiJack',
            'Encontre o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.home'),
            asset('frontend/assets/images/logo.png'));

        $propertiesForSale = Property::sale()->available()->limit(3)->get();
        $propertiesForRent = Property::rent()->available()->limit(3)->get();

        return view('web.home', [
            'head' => $head,
            'propertiesForSale' => $propertiesForSale,
            'propertiesForRent' => $propertiesForRent,
        ]);
    }

    public function spotlight()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - TiJack',
            'Encontre o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.spotlight'),
            asset('frontend/assets/images/logo.png'));
        return view('web.spotlight', [
            'head' => $head,
        ]);
    }

    public function experience()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - Experiências',
            'Viva a experiência de encontra o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.experience'),
            asset('frontend/assets/images/logo.png'));

        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::whereNotNull('experience')->get();

        return view('web.filter', [
            'head' => $head,
            'properties' => $properties
        ]);
    }

    public function experienceCategory(Request $request)
    {
        $filter = new FilterController();
        $filter->clearAllData();

        if ($request->slug == 'cobertura') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência de morar na cobertura...',
                route('web.experienceCategory', ['category' => 'cobertura']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'Cobertura')->get();
        } elseif ($request->slug == 'alto-padrao') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência de morar no Alto Padrão...',
                route('web.experienceCategory', ['category' => 'alto-padrao']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'Alto Padrão')->get();
        } elseif ($request->slug == 'de-frente-para-o-mar') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência de morar de frente para o mar...',
                route('web.experienceCategory', ['category' => 'de-frente-para-o-mar']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'De frente para o mar')->get();
        } elseif ($request->slug == 'condominio-fechado') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência de morar de em um condomínio fechado...',
                route('web.experienceCategory', ['category' => 'condominio-fechado']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'Condomínio Fechado')->get();
        } elseif ($request->slug == 'compacto') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência de morar em um compacto...',
                route('web.experienceCategory', ['category' => 'compacto']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'Compacto')->get();
        } elseif ($request->slug == 'lojas-e-salas') {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência e conforto nas melhores lojas com as melhores salas...',
                route('web.experienceCategory', ['category' => 'lojas-e-salas']),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::where('experience', 'Lojas e salas')->get();
        } else {
            $head = $this->seo->render(
                env('APP_NAME') . ' - Experiências',
                'Viva a experiência ao encontrar o imóvel dos seus sonhos...',
                route('web.experience'),
                asset('frontend/assets/images/logo.png'));
            $properties = Property::whereNotNull('experience')->get();
        }

        return view('web.filter', [
            'head' => $head,
            'properties' => $properties
        ]);
    }

    public function contact()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - Contato',
            'Entre em contato para encontrar o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.contact'),
            asset('frontend/assets/images/logo.png'));

        return view('web.contact', [
            'head' => $head,
        ]);
    }

    public function rent()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - Quero Alugar',
            'Alugue o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.rent'),
            asset('frontend/assets/images/logo.png'));

        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::rent()->available()->get();
        return view('web.filter', [
            'head' => $head,
            'properties' => $properties,
            'type' => 'rent'
        ]);
    }

    public function rentProperty(Request $request)
    {
        $property = Property::where('slug', $request->slug)->first();

        $head = $this->seo->render(
            env('APP_NAME') . ' - Quero Alugar',
            $property->headline ?? $property->title,
            route('web.rentProperty', ['property' => str_slug($property->title)]),
            $property->cover());
        return view('web.property', [
            'head' => $head,
            'property' => $property,
            'type' => 'rent'
        ]);
    }

    public function buy()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - Quero Comprar',
            'Compre o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.buy'),
            asset('frontend/assets/images/logo.png'));

        $filter = new FilterController();
        $filter->clearAllData();

        $properties = Property::sale()->available()->get();
        return view('web.filter', [
            'head' => $head,
            'properties' => $properties,
            'type' => 'sale'
        ]);
    }

    public function buyProperty(Request $request)
    {
        $property = Property::where('slug', $request->slug)->first();
        return view('web.property', [
            'property' => $property,
            'type' => 'sale'
        ]);
    }

    public function filter()
    {
        $head = $this->seo->render(
            env('APP_NAME') . ' - TiJack',
            'Encontre o imóvel dos seus sonhos na melhor imobiliária e mais completa de Vitória da Conquista',
            route('web.filter'),
            asset('frontend/assets/images/logo.png'));

        $filter = new FilterController();
        $itemProperties = $filter->createQuery('id');

        foreach ($itemProperties as $property) {
            $properties[] = $property->id;
        }
        if (!empty($properties)) {
            $properties = Property::whereIn('id', $properties)->get();
        } else {
            $properties = Property::all();
        }
        return view('web.filter', [
            'head' => $head,
            'properties' => $properties
        ]);
    }

    public function sendEmail(ContactRequest $request)
    {
        $data = [
            'reply_name' => $request->name,
            'reply_email' => $request->email,
            'cell' => $request->cell,
            'message' => $request->message,
        ];

        $primeiroNome = explode(" ", $request->name);

        try {
            if (isset($_COOKIE['imobSiteSendPost']) and $_COOKIE['imobSiteSendPost'] >= 10) {
                $primeiroNome[0] = $request->name == '' ? "M" : $primeiroNome[0] . ", m";
                return back()->with([
                    'message' => "<i class='icon-meh-o'></i><br><br><strong>{$primeiroNome[0]}ensagens d+ por hoje!!!</strong><br /><br />Fique tranquilo, já recebemos suas mensagens e em breve retornamos o contato.",
                    'type' => 'alert',
                ]);
            } else {
                Mail::send(new Contact($data));
                if (!isset($_COOKIE['imobSiteSendPost'])) :
                    $value = 1;
                    setcookie('imobSiteSendPost', $value, (time() + (3600 * 24)));
                else :
                    $value = $_COOKIE['imobSiteSendPost'] + 1;
                    setcookie('imobSiteSendPost', $value, (time() + (3600 * 24)));
                endif;

                return back()->with([
                    'message' => '<i class="icon-smile-o"></i><br><br><strong>Sucesso!!! obrigado pelo contato ' . $primeiroNome[0] . '.</strong><br /><br />Em breve estaremos respondendo você!',
                    'type' => 'accept',
                ]);
            }
        } catch (Exception $e) {
            return back()->with([
                'message' => '<strong>Oooops!!! Há algo errado.</strong><br /><br />Desculpe, não conseguimos enviar sua mensagem por aqui. Envie um email diretamente para:<br /> <strong>geoagro@geoagrovca.com.br</strong>',
                'type' => 'error',
            ]);
        }
    }

    public function sendEmailSuccess()
    {
        return view('web.contact_success');
    }
}
