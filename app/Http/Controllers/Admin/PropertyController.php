<?php

namespace Imob\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Imob\Http\Controllers\Controller;
use Imob\Http\Requests\Admin\PropertyRequest;
use Imob\Property;
use Imob\PropertyImage;
use Imob\suporte\Cropper;
use Imob\User;

class PropertyController extends Controller
{
    private $property;

    public function __construct(Property $property)
    {
        $this->property = $property;
    }

    public function index()
    {
        $properties = $this->property->orderBy('id', 'DESC')->get();
        return view('admin.properties.index', [
            'properties' => $properties
        ]);
    }

    public function create()
    {
        $users = User::orderBy('name')->get();
        return view('admin.properties.create', [
            'users' => $users
        ]);
    }

    public function store(PropertyRequest $request)
    {
        $propertyCreate = $this->property->create($request->all());

        $propertyCreate->setSlug();

        $validator = Validator::make($request->only('files'), ['files.*' => 'image']);

        if($validator->fails() === true){
            return redirect()->back()->withInput()->with(['color' => 'orange', 'message' => 'Todas imagnes devem ser do tipo jpg, jpeg, png ou svg!']);
        }

        if ($request->allFiles()) {
            foreach ($request->allFiles()['files'] as $image) {
                $propertyImage = new PropertyImage();
                $propertyImage->property = $propertyCreate->id;
                $propertyImage->path = $image->store('properties/' . $propertyCreate->id);
                $propertyImage->save();
                unset($propertyImage);
            }
        }

        return redirect()->route('admin.properties.edit', [
            'property' => $propertyCreate->id
        ])->with([
            'color' => 'green',
            'message' => 'Imóvel cadastrado com sucesso!'
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $property = $this->property->where('id', $id)->first();
        $users = User::orderBy('name')->get();

        return view('admin.properties.edit', [
            'property' => $property,
            'users' => $users,
        ]);
    }

    public function update(PropertyRequest $request, $id)
    {
        $property = $this->property->where('id', $id)->first();

        $property->setSaleAttribute($request->sale);
        $property->setRentAttribute($request->rent);
        $property->setAirConditioningAttribute($request->air_conditioning);
        $property->setBarAttribute($request->bar);
        $property->setLibraryAttribute($request->library);
        $property->setBarbecueGrillAttribute($request->barbecue_grill);
        $property->setAmericanKitchenAttribute($request->american_kitchen);
        $property->setFittedKitchenAttribute($request->fitted_kitchen);
        $property->setPantryAttribute($request->pantry);
        $property->setEdiculeAttribute($request->edicule);
        $property->setOfficeAttribute($request->office);
        $property->setBathtubAttribute($request->bathtub);
        $property->setFireplaceAttribute($request->fireplace);
        $property->setLavatoryAttribute($request->lavatory);
        $property->setFurnishedAttribute($request->furnished);
        $property->setPoolAttribute($request->pool);
        $property->setSteamRoomAttribute($request->steam_room);
        $property->setViewOfTheSeaAttribute($request->view_of_the_sea);

        $property->fill($request->all());

        $validator = Validator::make($request->only('files'), ['files.*' => 'image']);

        if($validator->fails() === true){
            return redirect()->back()->withInput()->with(['color' => 'orange', 'message' => 'Todas imagnes devem ser do tipo jpg, jpeg, png ou svg!']);
        }

        if (!$property->save()) {
            return redirect()->back()->withInput()->withErrors();
        }
        $property->setSlug();
        if ($request->allFiles()) {
            foreach ($request->allFiles()['files'] as $image) {
                $propertyImage = new PropertyImage();
                $propertyImage->property = $property->id;
                $propertyImage->path = $image->store('properties/' . $property->id);
                $propertyImage->save();
                unset($propertyImage);
            }
        }

        return redirect()->route('admin.properties.edit', [
            'property' => $property->id
        ])->with([
            'color' => 'green',
            'message' => 'Imóvel atualizado com sucesso!'
        ]);
    }

    public function destroy($id)
    {
        //
    }

    public function ImageSetCover(Request $request)
    {
        $imageSetCover = PropertyImage::where('id', $request->image)->first();
        $allImage = PropertyImage::where('property', $imageSetCover->property)->get();

        foreach ($allImage as $image) {
            $image->cover = null;
            $image->save();
        }
        $imageSetCover->cover = true;
        $imageSetCover->save();

        $json = [
            'success' => true
        ];
        return response()->json($json);
    }

    public function imageRemove(Request $request)
    {
        $imageRemove = PropertyImage::where('id', $request->image)->first();

        if($imageRemove->cover == 1){
            $json = [
                'error' => true
            ];
            return response()->json($json);
        }else{
            Storage::delete($imageRemove->path);
            Cropper::flush($imageRemove->path);
            $imageRemove->delete();

            $json = [
                'success' => true
            ];
            return response()->json($json);
        }

    }
}
