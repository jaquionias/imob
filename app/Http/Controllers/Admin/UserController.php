<?php

namespace Imob\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Imob\Http\Controllers\Controller;
use Imob\Http\Requests\Admin\UserRequest;
use Imob\suporte\Cropper;
use Imob\User;

class UserController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->all();
        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    public function team()
    {
        $users = $this->user->where('admin', 1)->get();
        return view('admin.users.team', [
            'users' => $users
        ]);
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $userCreate = $this->user->create($request->all());
        if (!empty($request->file('cover'))) {
            $userCreate->cover = $request->file('cover')->store('user');

            $userCreate->save();
        }
        return redirect()->route('admin.users.edit', [
            'users' => $userCreate->id
        ])->with([
            'color' => 'green',
            'message' => 'Cliente cadastrado com sucesso!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $user = $this->user->where('id', $id)->first();
        return view('admin.users.edit', [
            'user' => $user
        ]);
    }

    public function update(UserRequest $request, $id)
    {
        $user = $this->user->where('id', $id)->first();
        $user->setLessorAttribute($request->lessor);
        $user->setLesseeAttribute($request->lessee);
        if (!empty($request->file('cover'))) {
            Storage::delete($user->cover);
            Cropper::flush($user->cover);
            $user->cover = '';
        }
        $user->fill($request->all());
        if (!empty($request->file('cover'))) {
            $user->cover = $request->file('cover')->store('user');
        }
        if (!$user->save()) {
            return redirect()->back()->withInput()->withErrors();
        }
        return redirect()->route('admin.users.edit', [
            'users' => $user->id
        ])->with([
            'color' => 'green',
            'message' => 'Cliente atualizado com sucesso!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
