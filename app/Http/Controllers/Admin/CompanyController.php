<?php

namespace Imob\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Imob\Company;
use Imob\Http\Controllers\Controller;
use Imob\Http\Requests\Admin\CompanyRequest;
use Imob\User;

class CompanyController extends Controller
{
    private $company;

    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    public function index()
    {
        $companies = $this->company->all();
        return view('admin.companies.index', [
            'companies' => $companies
        ]);
    }

    public function create(Request $request)
    {
        $users = User::orderBy('name')->get();
        $user = null;
        if (!empty($request->user)) {
            $user = User::where('id', $request->user)->first();
        }
        return view('admin.companies.create', [
            'users' => $users,
            'selected' => $user
        ]);
    }

    public function store(CompanyRequest $request)
    {
        $comanyCreate = $this->company->create($request->all());

        return redirect()->route('admin.companies.edit', [
            'company' => $comanyCreate->id
        ])->with([
            'color' => 'green',
            'message' => 'Empresa cadastrada com sucesso!'
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $company = $this->company->where('id', $id)->first();
        $users = User::orderBy('name')->get();

        return view('admin.companies.edit', [
            'company' => $company,
            'users' => $users
        ]);
    }

    public function update(CompanyRequest $request, $id)
    {
        $company = $this->company->where('id', $id)->first();
        $company->fill($request->all());

        if (!$company->save()) {
            return redirect()->back()->withInput()->withErrors();
        }
        return redirect()->route('admin.companies.edit', [
            'company' => $company->id
        ])->with([
            'color' => 'green',
            'message' => 'Empresa atualizado com sucesso!'
        ]);
    }

    public function destroy($id)
    {
        //
    }
}
