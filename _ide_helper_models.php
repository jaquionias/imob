<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace Imob{
/**
 * Imob\Company
 *
 * @property int $id
 * @property \Imob\User $user
 * @property string $social_name
 * @property string $alias_name
 * @property string $document_company
 * @property string $document_company_secondary
 * @property string|null $zipcode
 * @property string|null $street
 * @property string|null $number
 * @property string|null $complement
 * @property string|null $neighborhood
 * @property string|null $state
 * @property string|null $city
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company query()
 */
	class Company extends \Eloquent {}
}

namespace Imob{
/**
 * Imob\Contract
 *
 * @property int $id
 * @property int $sale
 * @property int $rent
 * @property int $owner
 * @property int|null $owner_spouse
 * @property int|null $owner_company
 * @property int $acquirer
 * @property int|null $acquirer_spouse
 * @property int|null $acquirer_company
 * @property int $property
 * @property float $price
 * @property float $tribute
 * @property float $condominium
 * @property int $due_date
 * @property int $deadline
 * @property string $start_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $status
 * @property-read \Imob\Company|null $acquirerCompanyObject
 * @property-read \Imob\User|null $acquirerObject
 * @property-read \Imob\Company|null $ownerCompanyObject
 * @property-read \Imob\User|null $ownerObject
 * @property-read \Imob\Property|null $propertyObject
 * @property-write mixed $rent_price
 * @property-write mixed $sale_price
 * @method static \Illuminate\Database\Eloquent\Builder|Contract newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contract newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Contract query()
 */
	class Contract extends \Eloquent {}
}

namespace Imob{
/**
 * Imob\Property
 *
 * @property int $id
 * @property int|null $sale
 * @property int|null $rent
 * @property string $category
 * @property string $type
 * @property \Imob\User $user
 * @property string|null $sale_price
 * @property string|null $rent_price
 * @property string|null $tribute
 * @property string|null $condominium
 * @property string|null $description
 * @property int $bedrooms
 * @property int $suites
 * @property int $bathrooms
 * @property int $rooms
 * @property int $garage
 * @property int $garage_covered
 * @property int $area_total
 * @property int $area_util
 * @property string|null $zipcode
 * @property string|null $street
 * @property string|null $number
 * @property string|null $complement
 * @property string|null $neighborhood
 * @property string|null $state
 * @property string|null $city
 * @property int|null $air_conditioning
 * @property int|null $bar
 * @property int|null $library
 * @property int|null $barbecue_grill
 * @property int|null $american_kitchen
 * @property int|null $fitted_kitchen
 * @property int|null $pantry
 * @property int|null $edicule
 * @property int|null $office
 * @property int|null $bathtub
 * @property int|null $fireplace
 * @property int|null $lavatory
 * @property int|null $furnished
 * @property int|null $pool
 * @property int|null $steam_room
 * @property int|null $view_of_the_sea
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $status
 * @property-read \Illuminate\Database\Eloquent\Collection|\Imob\PropertyImage[] $images
 * @property-read int|null $images_count
 * @method static \Illuminate\Database\Eloquent\Builder|Property newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Property newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Property query()
 */
	class Property extends \Eloquent {}
}

namespace Imob{
/**
 * Imob\PropertyImage
 *
 * @property int $id
 * @property int $property
 * @property string $path
 * @property int|null $cover
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $url_cropped
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PropertyImage query()
 */
	class PropertyImage extends \Eloquent {}
}

namespace Imob{
/**
 * Imob\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $last_login_at
 * @property string|null $last_login_at_ip
 * @property int|null $lessor
 * @property int|null $lessee
 * @property string|null $genre
 * @property string $document
 * @property string|null $document_secondary
 * @property string|null $document_secondary_complement
 * @property string|null $date_of_birth
 * @property string|null $place_of_birth
 * @property string|null $civil_status
 * @property string|null $cover
 * @property string|null $occupation
 * @property float|null $income
 * @property string|null $company_work
 * @property string|null $zipcode
 * @property string|null $street
 * @property string|null $number
 * @property string|null $complement
 * @property string|null $neighborhood
 * @property string|null $state
 * @property string|null $city
 * @property string|null $telephone
 * @property string|null $cell
 * @property string|null $type_of_communion
 * @property string|null $spouse_name
 * @property string|null $spouse_genre
 * @property string $spouse_document
 * @property string|null $spouse_document_secondary
 * @property string|null $spouse_document_secondary_complement
 * @property string|null $spouse_date_of_birth
 * @property string|null $spouse_place_of_birth
 * @property string|null $spouse_occupation
 * @property string|null $spouse_income
 * @property string|null $spouse_company_work
 * @property int|null $admin
 * @property int|null $client
 * @property-read \Illuminate\Database\Eloquent\Collection|\Imob\Company[] $companies
 * @property-read int|null $companies_count
 * @property-read mixed $url_cover
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Imob\Property[] $properties
 * @property-read int|null $properties_count
 * @method static \Illuminate\Database\Eloquent\Builder|User lessees()
 * @method static \Illuminate\Database\Eloquent\Builder|User lessors()
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 */
	class User extends \Eloquent {}
}

